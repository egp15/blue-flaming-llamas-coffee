# README #

Blue Flaming Llamas Coffee program. 

### What is this repository for? ###

* This a program to order coffee. 

### How do I get set up? ###

* Put java files in to the same folder. 
* Add path to java. Compile all files. Run CoffeeMain.java 

* It will create text files 
* order.txt history.txt toppings.txt flavors.txt and briefly while a certain part program is running a tempFile.txt
* on its own. 

**
* From the program you will be able to 
* create an order of a coffee flavor and any number of toppings in the customer menu *
* add toppings and flavors in the managers menu *
* delete toppings and flavors in the managers menu *
* view order history in the managers menu *
**

### Contributers ###

* Joe Bumbaugh
* Lucas Fetters
* Eric Price
* David Robertson
* Tony Pavia
* Noah Lemmon