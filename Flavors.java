

import java.io.*;
import java.math.BigDecimal;

/**Class for the types "flavors" of coffee. 
 * 
 * 
 */
public class Flavors extends CoffeeInterface{
    
    private String coffeeName; 
    private String coffeeDescription; 
    private Double coffeePrice; 
    
    /**Default constructor
     * 
     */
    public void Flavors()
    {
        
    }
    
    /**Constructor that sets name. 
     * 
     * @param name 
     */
    public void Flavors(String name)
    {
        coffeeName = name; 
    }
    
    /**Constructor that sets all variables.  
     * 
     * @param name 
     * @param price
     * @param description
     */
    public void Flavors(String name, Double price, String description)
    {
        coffeeName = name; 
        coffeePrice = price; 
        coffeeDescription = description; 
    }
    
    
    /**Display flavors list. 
     * 
     */
    public void display()
    {
        
    }
    
    /**Get price for the selection 
     * @param selection 
     */
    public void price(int selection)
    {
        
    }
    
    /**Setter method for manager to set name.
     * 
     */
    public void setName(String name)
    {
        coffeeName = name; 
    }
    
    /**Setter method for manager to set description. 
     * 
     * @param description 
     */
    public void setDescription(String description)
    {
        coffeeDescription = description; 
    }
    
    /**Setter method for manager to set price. 
     * 
     * @param price 
     */
    public void setPrice(Double price)
    {
        coffeePrice = price; 
    }
    
    /**Stores data into text file
     * 
     */
    public void storeData()
    {
            BufferedWriter bw = null; 
            BigDecimal roundPrice = new BigDecimal(coffeePrice);
            roundPrice = roundPrice.setScale(2, BigDecimal.ROUND_HALF_UP);
            
            try
            {
                bw = new BufferedWriter(new FileWriter("Flavors.txt", true));
                bw.write(coffeeName + " " + roundPrice + " " + coffeeDescription);
                bw.newLine(); 
                bw.flush(); 
            }
            catch(IOException ioe)
            {
                ioe.printStackTrace(); 
            }
            finally
            {
                if(bw != null)
                {
                    try
                    {
                        bw.close(); 
                    }
                    catch(IOException ioe2)
                    {
                        System.out.println("File didn't close.");
                    }
                }
            }
        }
    
    
}
