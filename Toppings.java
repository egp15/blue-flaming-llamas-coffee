 

import java.io.*; 
import java.math.BigDecimal;

/**Class for toppings which can be added
 * to the coffee order in any amount and 
 * any variety. Created by the manager. 
 * 
 */
public class Toppings extends CoffeeInterface{

        private String toppingName;
        private String toppingDescription; 
        private double toppingPrice; 
        
        /** Default constructor for a topping. 
         *
         */
        public void toppings()
        {
            
        }
        
        /**Constructor that assigns name. 
         * 
         * @param name 
         */
        public void toppings(String name)
        {
            setName(name);
        }
        
        /**Constructor that assigns all variables for topping object. 
         * 
         * @param name 
         * @param price
         * @param description
         */
        public void toppings(String name, Double price, String description)
        {
            toppingName = name; 
            toppingPrice = price; 
            toppingDescription = description; 
        }

        
        /**Displays the toppings list. 
         * 
         */
        public void display()
        {
            
        }
        
        /**Gets the price for the selection.
         * 
         */
        public void price(int selection)
        {
            
        }
        
        /**Setter method for manager to set name.
         * 
         * @param name 
         */
        public void setName(String name)
        {
            toppingName = name; 
        }
        
        /**Setter method for manager to set description. 
         * 
         * @param description 
         */
        public void setDescription(String description)
        {
            toppingDescription = description; 
        }
        
        /**Setter method for manager to set price. 
         * 
         * @param price 
         */
        public void setPrice(Double price)
        {
            toppingPrice = price; 
        }
        
        /**Stores data into text file. 
         * 
         */
        public void storeData()
        {
            BufferedWriter bw = null; 
            BigDecimal roundPrice = new BigDecimal(toppingPrice);
            roundPrice = roundPrice.setScale(2, BigDecimal.ROUND_HALF_UP);
            
            try
            {
                bw = new BufferedWriter(new FileWriter("toppings.txt", true));
                bw.write(toppingName + " " + roundPrice + " " + toppingDescription);
                bw.newLine(); 
                bw.flush(); 
            }
            catch(IOException ioe)
            {
                ioe.printStackTrace(); 
            }
            finally
            {
                if(bw != null)
                {
                    try
                    {
                        bw.close(); 
                    }
                    catch(IOException ioe2)
                    {
                        System.out.println("File didn't close.");
                    }
                }
            }
        }
        
        
}
