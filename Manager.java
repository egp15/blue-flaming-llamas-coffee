

import java.io.*;
import java.util.Scanner;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**The manager screen for our interface. 
 * 
 * @author blue-flaming-llamas
 */
public class Manager 
{
	/**Shows the history.txt file. 
         * 
         */
	public void ShowReport()
	{ 	
        	File file = new File("history.txt");
       		Scanner in = null;
        	try {
           	 in = new Scanner(file);
           	 while(in.hasNext())
           		 {
              		     String line=in.nextLine();
               		     System.out.println(line);
           		 }
        	} 
		catch (FileNotFoundException e) {
           	 
           	 e.printStackTrace();
        	}
		
	}
        
        /**Allows manager to add/delete flavors 
         * 
         */
	public void EditMenu()
	{
                // - - Variables for edit menu method. 
		String InputName;
		Double PriceInput = 0.0;
		Integer UserInput = 0;
                String Description = null;
                Scanner reader = new Scanner(System.in);
                
                // - - While loop for user selection. 
		while (UserInput != 5)
		{
			System.out.println("Enter 1) To Add a Topping; 2) To Delete a Topping; 3) To Add a Flavor; 4) To Delete a Flavor; 5) To Quit");
			UserInput = reader.nextInt();
                        reader.nextLine();  
                        
                        // - - Add a topping menu
			if (UserInput == 1) 
			{
				System.out.println("What is the name of the topping?");
				InputName = reader.next();
				Toppings NewTopping = new Toppings(); 
     			 	NewTopping.setName(InputName);

                                
                                System.out.println("What is the price?");
				PriceInput = reader.nextDouble();
                                reader.nextLine();
  	 	  		NewTopping.setPrice(PriceInput); 
                                
				System.out.println("Describe it.");
                                Description = reader.nextLine();
    			    	NewTopping.setDescription(Description);
                                NewTopping.storeData(); 
                                UserInput = 0;
			}
                        
                        // - - Delete a topping menu 
			else if (UserInput == 2) 
			{
                            String fileName = "toppings.txt";
                            String line = null; 
                            String fileLine = null; 
                            String toppingDelete = null;
                            char[] chStr; 
                            char[] delTop; 
                            int topLength = 0; 
                            int count = 1; 
                            boolean check = true; 
                            BufferedWriter bw = null; 
                            Scanner sc = new Scanner(System.in);
                            
                            
                            System.out.println("Please enter the "
                                    + "topping you would like to delete.");
                            
                            toppingDelete = sc.next();
                            topLength = toppingDelete.length(); 
                            delTop = toppingDelete.toCharArray(); 
                            
                            // - - try block for comparing to find 
                            // - - desired topping, writing a temp file 
                            // - - without deleted topping and overwriting 
                            // - - toppings.txt 
                            try
                            {
                                File file = new File("tempFile.txt");
                                bw = new BufferedWriter(new FileWriter("tempFile.txt", true));
                                FileReader fileReader = new FileReader(fileName);
                                BufferedReader bufferedReader = new BufferedReader(fileReader);
                                
                                while((line = bufferedReader.readLine()) != null)
                                {
                                    chStr = line.toCharArray();
                                    while(check == true && count != topLength)
                                    {
                                        check = (chStr[count - 1] == delTop[count - 1]);
                                        count++;
                                    }
                                    if(count != topLength)
                                    {
                                        bw.write(line);
                                        bw.newLine(); 
                                        bw.flush(); 
                                    }
                                    count = 1; 
                                    check = true; 
                                }
                                bw.close(); 
                                fileReader.close();
                                bufferedReader.close();
                                File f = new File("toppings.txt");
                                f.delete(); 
                                File oN = new File("tempFile.txt");
                                File nN = new File("toppings.txt");
                                
                                if(oN.renameTo(nN))
                                {
                                    System.out.println("File renamed.");
                                }
                                else
                                {
                                    System.out.println("Error.");
                                }
                            }
                            catch(IOException e)
                            {
                                System.out.println("something wrong in delete.");
                            }
                            UserInput = 0;
                        }
                        
                        // - - User menu for adding flavor 
			else if (UserInput == 3) 
			{
				System.out.println("What is the name of the flavor?");
				InputName = reader.next();
				Flavors NewFlavor = new Flavors(); 
     			 	NewFlavor.setName(InputName);
    
                                System.out.println("What is the price?");
				PriceInput = reader.nextDouble();
                                reader.nextLine();
  	 	  		NewFlavor.setPrice(PriceInput); 
                                
				System.out.println("Describe it.");
				Description = reader.nextLine();
    			    	NewFlavor.setDescription(Description);
                                NewFlavor.storeData();  
                                UserInput = 0;
			}
                        
                        // - - User menu for deleting specific flavors. 
			else if (UserInput == 4)
			{
                            String fileName = "flavors.txt";
                            String line = null; 
                            String fileLine = null; 
                            String flavorDelete = null;
                            char[] chStr; 
                            char[] delFlav; 
                            int flavLength = 0; 
                            int count = 1; 
                            boolean check = true; 
                            BufferedWriter bw = null; 
                            Scanner sc = new Scanner(System.in);
                            
                            
                            System.out.println("Please enter the "
                                    + "flavor you would like to delete.");
                            
                            flavorDelete = sc.next();
                            flavLength = flavorDelete.length(); 
                            delFlav = flavorDelete.toCharArray(); 
                            
                            // - - try block for comparing to find 
                            // - - desired flavor, writing a temp file 
                            // - - without deleted flavor and overwriting 
                            // - - flavors.txt 
                            try
                            {
                                File file = new File("tempFile.txt");
                                bw = new BufferedWriter(new FileWriter("tempFile.txt", true));
                                FileReader fileReader = new FileReader(fileName);
                                BufferedReader bufferedReader = new BufferedReader(fileReader);
                                
                                while((line = bufferedReader.readLine()) != null)
                                {
                                    chStr = line.toCharArray();
                                    while(check == true && count != flavLength)
                                    {
                                        check = (chStr[count - 1] == delFlav[count - 1]);
                                        count++;
                                    }
                                    if(count != flavLength)
                                    {
                                        bw.write(line);
                                        bw.newLine(); 
                                        bw.flush(); 
                                    }
                                    count = 1; 
                                    check = true; 
                                }
                                bw.close(); 
                                fileReader.close();
                                bufferedReader.close();
                                File f = new File("flavors.txt");
                                f.delete(); 
                                File oN = new File("tempFile.txt");
                                File nN = new File("flavors.txt");
                                
                                if(oN.renameTo(nN))
                                {
                                    System.out.println("File renamed.");
                                }
                                else
                                {
                                    System.out.println("Error.");
                                }
                            }
                            catch(IOException e)
                            {
                                System.out.println("something wrong in delete.");
                            }
                            UserInput = 0;
			}
                
		}
	}
}
