

import java.io.IOException;
import java.io.PrintWriter;

/**Interface for flavors and toppings.
 *
 * 
 */
public abstract class CoffeeInterface {
    
    /**Prints the coffee flavor or topping
     * and the description added by
     * the manager. 
     * 
     */
    public abstract void display();
    
    /**Prints the price of the item. 
     * @param selection 
     */
    public abstract void price(int selection);
    
    /**Sets the name of the item.
     * @param name
     */
    public abstract void setName(String name);
    
    /**Setter method for manager to set price. 
     * 
     * @param price 
     */
    public abstract void setPrice(Double price);
    
    /**Setter method for manager to set description. 
     * 
     * @param description 
     */
    public abstract void setDescription(String description);
    
    /**Stores the information in the file.
     * 
     */
    public abstract void storeData();
    
    /**Clears the text file. 
     * 
     */
    public void clearFile()
    {
        try
        {    
        PrintWriter writer = new PrintWriter("Flavors.txt");
        writer = new PrintWriter("Toppings.txt");
        writer.close();
        }
        catch(IOException e)
        {
            System.out.println(e + "failed.");
        }
    }
    
}
