import java.math.RoundingMode;
import java.util.*;
import java.io.*;
import java.io.FileNotFoundException;
import java.math.BigDecimal;

public class Customer 
{
    public char[] chars = new char[8];  //a string for the array
    String date = new String(chars);    //constantly 8 characters
    String name;                        //string for name
    double toppings = 0;                //price of toppings
    double total = 0;                   //price of total
    
    public Scanner reader = null;
    public String getDate()
    {
        return date;
    }
    
    public int getNumFlavors()  //looks at how many lines there are in the flavors.txt and determines how many flavors there are
    {
        File flavors = new File("Flavors.txt");
        try{
            reader = new Scanner(flavors);
        }
        catch(FileNotFoundException e){
            e.printStackTrace();
        }
        
        int n = 0;
        
        if(flavors.length() == 0)
        {
            n = 0;
        }
        else
        {
            while (reader.hasNextLine())
            {
                reader.nextLine();
                n++;
            }
        }
        reader.close(); 
        
        return n;
    }
    
    public int getNumToppings() //returns the number of toppings
    {
        File toppings = new File("toppings.txt");
        try{
            reader = new Scanner(toppings);
        }
        catch(FileNotFoundException e){
            e.printStackTrace();
        }
        
        int n = 0;
        
        if(toppings.length() == 0)
        {
            n = 0;
        }
        else
        {
            while (reader.hasNextLine())
            {
                reader.nextLine();
                n++;
            }
        }
        reader.close(); 
        return n;
    }
    
    public void createOrder()
    {
        int[] numFlavors = new int[getNumFlavors()];            //the possible user choice for flavor
        String[] flavorNames = new String[getNumFlavors()];     //array containing every flavor name
        double[] priceFlavors = new double[getNumFlavors()];    //array containing price for every flavor
        
        int[] numToppings = new int[getNumToppings()];
        String[] toppingNames = new String[getNumToppings()];
        int[] toppingOrder = new int[getNumToppings()];
        double[] priceToppings = new double[getNumToppings()];     //array that is the size of number of toppings
        
        for(int i = 0; i < getNumToppings(); i++)                   //every value is set to 0 in the array
        {                                             
            toppingOrder[i] = 0;
        }
        
        File flavors = new File("Flavors.txt");
        try{
            reader = new Scanner(flavors);
        }
        catch(FileNotFoundException e){
            e.printStackTrace();
        }
        
        
        System.out.println("Here is a list of flavors, its price, and a description for each!");
        int n = 1;
        
        if(flavors.length() == 0)
        {
            System.out.println("No flavors on menu!");
        }
        else
        {
            int i = 0;
            String[] temp;
            while (reader.hasNextLine())
            {
                String line = reader.nextLine();
                System.out.print("(");
                System.out.print(n);
                numFlavors[i] = n;
                System.out.print(")");
                System.out.print (line);
                temp = line.split(" ");
                flavorNames[i] = temp[0];
                priceFlavors[i] = Double.parseDouble(temp[1]);  //price is casted to a double
                System.out.print("\n");
                i++;
                n++;
            }
        }
        
        reader.close();
        System.out.print("\n");
        
        File topping = new File("toppings.txt");
        try{
            reader = new Scanner(topping);
        }
        catch(FileNotFoundException e){
            e.printStackTrace();
        }
        
        
        System.out.println("Here is a list of toppings, its price, and a description for each!");
        n = 1;
        
        if(topping.length() == 0)
        {
            System.out.println("No toppings on menu!");
        }
        else
        {
            int i = 0;
            String[] temp;
            while (reader.hasNextLine())
            {
                String line = reader.nextLine();
                System.out.print("(");
                System.out.print(n);
                numToppings[i] = n;
                System.out.print(")");
                System.out.print (line);
                temp = line.split(" ");
                toppingNames[i] = temp[0];
                priceToppings[i] = Double.parseDouble(temp[1]);
                System.out.print("\n");
                n++;
                i++;
            }
        }
        reader.close();
        
        int flavChoice, topChoice, numTop;
        Scanner d = new Scanner(System.in);
        
        System.out.print("Enter your name: ");
        name = d.nextLine();
        
        System.out.print("Enter today's date (mm/dd/yy)... ");
        date = d.nextLine();
        
        System.out.print("Enter the number corresponding to the flavor you would like: ");
        flavChoice = d.nextInt();
        total = priceFlavors[flavChoice - 1];
        
        System.out.print("Enter the topping you would like. If you don't want any, enter 0: ");
        topChoice = d.nextInt();
        while(topChoice != 0)
        {
            System.out.print("How many of that topping would you like?: ");
            numTop = d.nextInt();
            toppingOrder[topChoice - 1] = numTop;
            toppings = toppings + (priceToppings[topChoice - 1] * numTop);          //the array of 0s is set at index of the user choice to the value of how much of that topping they want
            
            System.out.print("To add more toppings, select from the menu above. If you are done adding toppings, enter 0: ");
            topChoice = d.nextInt();
        }
        total = total + toppings;
        System.out.print("Your Total is: $");
        System.out.print(saveOrder(flavChoice, flavorNames, toppingNames, toppingOrder));
        System.out.print("\n");
        
        
    }
     
    public BigDecimal saveOrder(int flavChoice, String[] flavorNames, String[] toppingNames, int[] toppingOrder)
    {
        BigDecimal roundTot = new BigDecimal(total);
        BigDecimal roundTop = new BigDecimal(toppings);
        roundTop = roundTop.setScale(2, BigDecimal.ROUND_HALF_UP);
        roundTot = roundTot.setScale(2, BigDecimal.ROUND_HALF_UP);      //doubles are turned into big decimals for money
        
	BufferedWriter bw = null;
        FileWriter pr = null;
        
            try
            {
                bw = new BufferedWriter(new FileWriter("history.txt", true));
                pr = new FileWriter("orders.txt");
                pr.write("");
                bw.write(date + " " + flavorNames[flavChoice - 1] + " ");
                pr.write(date + " " + flavorNames[flavChoice - 1] + " ");
                for(int i = 0; i < getNumToppings(); i++)
                {
                    if(toppingOrder[i] > 0)     //if the user didn't order any of that topping, it doesn't print anything
                    {
                        bw.write(Integer.toString(toppingOrder[i]));
                        bw.write(" " + toppingNames[i] + " ");
                        pr.write(Integer.toString(toppingOrder[i]));
                        pr.write(" " + toppingNames[i] + " ");
                    }
                }
                bw.write(name + " ");
                bw.write(roundTop + " ");
                bw.write(roundTot + " ");
                
                pr.write(name + " ");
                pr.write(roundTop + " ");
                pr.write(roundTot + " ");
                
                bw.newLine(); 
                bw.flush();
                pr.flush();
                
                bw.close();
                pr.close();
            }
            catch(IOException ioe)
            {
                ioe.printStackTrace(); 
            }
            finally
            {
                if(bw != null)
                {
                    try
                    {
                        bw.close(); 
                    }
                    catch(IOException ioe2)
                    {
                        System.out.println("File didn't close.");
                    }
                }
            }	
        return roundTot;
    }
}
